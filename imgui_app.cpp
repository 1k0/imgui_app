#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <Gl/GL.h>

#include "common.h"
#include "imgui/imgui.h"

#include "imgui_app.h"

#define internal static
#define external extern

enum ImGuiStatus : u32
{
    IMGUISTATUS_OK = 0,
    IMGUISTATUS_ERROR_WINDOW_HANDLE,
    IMGUISTATUS_ERROR_PIXEL_FORMAT,
    IMGUISTATUS_ERROR_GL_CONTEXT,
    IMGUISTATUS_ERROR_GL_FUNCTION_LOAD,
    IMGUISTATUS_ERROR_RENDERER_INIT,
    IMGUISTATUS_COUNT,
};

const char* ImGuiStatusToString[IMGUISTATUS_COUNT] =
{
    "ok",
    "window could not be created",
    "pixel format not supported",
    "gl context could not be created",
    "gl function could not be loaded",
    "renderer init error",
};

//#OpenGL2---------------------------------------------------------------------

typedef GLuint GlHandle;
typedef char GLchar;
#ifdef _WIN64
    typedef signed long long int GLsizeiptr;
    typedef signed long long int GLintptr;
#else
    typedef signed long int GLsizeiptr;
    typedef signed long int GLintptr;
#endif

#define APIENTRYP APIENTRY *
#define GL_ARRAY_BUFFER                   0x8892
#define GL_STREAM_DRAW                    0x88E0
#define GL_DYNAMIC_DRAW                   0x88E8
#define GL_FRAGMENT_SHADER                0x8B30
#define GL_VERTEX_SHADER                  0x8B31
#define GL_ELEMENT_ARRAY_BUFFER           0x8893
#define GL_TEXTURE0                       0x84C0

typedef HGLRC (APIENTRYP PFNWGLCREATECONTEXT) (HDC);
typedef BOOL (APIENTRYP PFNWGLMAKECURRENT) (HDC, HGLRC);
typedef PROC (APIENTRYP PFNWGLGETPROCADDRESS) (LPCSTR);

typedef void (APIENTRYP PFNGLBINDTEXTUREPROC) (GLenum target, GLuint texture);
typedef void (APIENTRYP PFNGLBLENDFUNCPROC) (GLenum sfactor, GLenum dfactor);
typedef void (APIENTRYP PFNGLCLEARPROC) (GLbitfield mask);
typedef void (APIENTRYP PFNGLCLEARCOLORPROC) (GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
typedef void (APIENTRYP PFNGLDISABLEPROC) (GLenum cap);
typedef void (APIENTRYP PFNGLDRAWELEMENTSPROC) (GLenum mode, GLsizei count, GLenum type, const void *indices);
typedef void (APIENTRYP PFNGLENABLEPROC) (GLenum cap);
typedef void (APIENTRYP PFNGLGENTEXTURESPROC) (GLsizei n, GLuint *textures);
typedef void (APIENTRYP PFNGLPIXELSTOREIPROC) (GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLPOLYGONMODEPROC) (GLenum face, GLenum mode);
typedef void (APIENTRYP PFNGLSCISSORPROC) (GLint x, GLint y, GLsizei width, GLsizei height);
typedef void (APIENTRYP PFNGLTEXIMAGE2DPROC) (GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const void *pixels);
typedef void (APIENTRYP PFNGLTEXPARAMETERIPROC) (GLenum target, GLenum pname, GLint param);
typedef void (APIENTRYP PFNGLVIEWPORTPROC) (GLint x, GLint y, GLsizei width, GLsizei height);

typedef GLuint (APIENTRYP PFNGLCREATESHADERPROC) (GLenum type);
typedef void (APIENTRYP PFNGLSHADERSOURCEPROC) (GLuint shader, GLsizei count, const GLchar *const*string, const GLint *length);
typedef void (APIENTRYP PFNGLCOMPILESHADERPROC) (GLuint shader);
typedef void (APIENTRYP PFNGLDELETESHADERPROC) (GLuint shader);
typedef void (APIENTRYP PFNGLDETACHSHADERPROC) (GLuint program, GLuint shader);
typedef GLuint (APIENTRYP PFNGLCREATEPROGRAMPROC) (void);
typedef void (APIENTRYP PFNGLATTACHSHADERPROC) (GLuint program, GLuint shader);
typedef void (APIENTRYP PFNGLLINKPROGRAMPROC) (GLuint program);
typedef GLint (APIENTRYP PFNGLGETUNIFORMLOCATIONPROC) (GLuint program, const GLchar *name);
typedef GLint (APIENTRYP PFNGLGETATTRIBLOCATIONPROC) (GLuint program, const GLchar *name);
typedef void (APIENTRYP PFNGLUNIFORM1IPROC) (GLint location, GLint v0);
typedef void (APIENTRYP PFNGLACTIVETEXTUREPROC) (GLenum texture);
typedef void (APIENTRYP PFNGLGENBUFFERSPROC) (GLsizei n, GLuint *buffers);
typedef void (APIENTRYP PFNGLBINDBUFFERPROC) (GLenum target, GLuint buffer);
typedef void (APIENTRYP PFNGLBUFFERDATAPROC) (GLenum target, GLsizeiptr size, const void *data, GLenum usage);
typedef void (APIENTRYP PFNGLBUFFERSUBDATAPROC) (GLenum target, GLintptr offset, GLsizeiptr size, const void *data);
typedef void (APIENTRYP PFNGLVERTEXATTRIBPOINTERPROC) (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void *pointer);
typedef void (APIENTRYP PFNGLENABLEVERTEXATTRIBARRAYPROC) (GLuint index);
typedef void (APIENTRYP PFNGLGETSHADERINFOLOGPROC) (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (APIENTRYP PFNGLGETPROGRAMINFOLOGPROC) (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog);
typedef void (APIENTRYP PFNGLUSEPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLUNIFORMMATRIX4FVPROC) (GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);
typedef void (APIENTRYP PFNGLDELETEPROGRAMPROC) (GLuint program);
typedef void (APIENTRYP PFNGLDELETEBUFFERSPROC) (GLsizei n, const GLuint *buffers);

//Vanilla gl
internal PFNWGLCREATECONTEXT WglCreateContext;
internal PFNWGLMAKECURRENT WglMakeCurrent;
internal PFNWGLGETPROCADDRESS WglGetProcAddress;

internal PFNGLBINDTEXTUREPROC GlBindTexture;
internal PFNGLBLENDFUNCPROC GlBlendFunc;
internal PFNGLCLEARPROC GlClear;
internal PFNGLCLEARCOLORPROC GlClearColor;
internal PFNGLDISABLEPROC GlDisable;
internal PFNGLDRAWELEMENTSPROC GlDrawElements;
internal PFNGLENABLEPROC GlEnable;
internal PFNGLGENTEXTURESPROC GlGenTextures;
internal PFNGLPIXELSTOREIPROC GlPixelStorei;
internal PFNGLPOLYGONMODEPROC GlPolygonMode;
internal PFNGLSCISSORPROC GlScissor;
internal PFNGLTEXIMAGE2DPROC GlTexImage2D;
internal PFNGLTEXPARAMETERIPROC GlTexParameteri;
internal PFNGLVIEWPORTPROC GlViewport;

//Extensions
internal PFNGLCREATESHADERPROC GlCreateShader;
internal PFNGLSHADERSOURCEPROC GlShaderSource;
internal PFNGLCOMPILESHADERPROC GlCompileShader;
internal PFNGLDELETESHADERPROC GlDeleteShader;
internal PFNGLDETACHSHADERPROC GlDetachShader;
internal PFNGLCREATEPROGRAMPROC GlCreateProgram;
internal PFNGLATTACHSHADERPROC GlAttachShader;
internal PFNGLLINKPROGRAMPROC GlLinkProgram;
internal PFNGLGETUNIFORMLOCATIONPROC GlGetUniformLocation;
internal PFNGLGETATTRIBLOCATIONPROC GlGetAttribLocation;
internal PFNGLUNIFORM1IPROC GlUniform1i;
internal PFNGLACTIVETEXTUREPROC GlActiveTexture;
internal PFNGLGENBUFFERSPROC GlGenBuffers;
internal PFNGLBINDBUFFERPROC GlBindBuffer;
internal PFNGLBUFFERDATAPROC GlBufferData;
internal PFNGLBUFFERSUBDATAPROC GlBufferSubData;
internal PFNGLVERTEXATTRIBPOINTERPROC GlVertexAttribPointer;
internal PFNGLENABLEVERTEXATTRIBARRAYPROC GlEnableVertexAttribArray;
internal PFNGLGETSHADERINFOLOGPROC GlGetShaderInfoLog;
internal PFNGLGETPROGRAMINFOLOGPROC GlGetProgramInfoLog;
internal PFNGLUSEPROGRAMPROC GlUseProgram;
internal PFNGLUNIFORMMATRIX4FVPROC GlUniformMatrix4fv;
internal PFNGLDELETEPROGRAMPROC GlDeleteProgram;
internal PFNGLDELETEBUFFERSPROC GlDeleteBuffers;

internal HMODULE opengl;

internal bool ImGuiWglInit()
{
    opengl = LoadLibraryA("opengl32.dll");

    return (true
        && (WglCreateContext = (PFNWGLCREATECONTEXT) GetProcAddress(opengl, "wglCreateContext"))
        && (WglMakeCurrent = (PFNWGLMAKECURRENT) GetProcAddress(opengl, "wglMakeCurrent"))
        && (WglGetProcAddress = (PFNWGLGETPROCADDRESS) GetProcAddress(opengl, "wglGetProcAddress"))
    );
}

internal bool ImGuiGl1Init()
{
    return (true
        && (GlBindTexture = (PFNGLBINDTEXTUREPROC) GetProcAddress(opengl, "glBindTexture"))
        && (GlBlendFunc = (PFNGLBLENDFUNCPROC) GetProcAddress(opengl, "glBlendFunc"))
        && (GlClear = (PFNGLCLEARPROC) GetProcAddress(opengl, "glClear"))
        && (GlClearColor = (PFNGLCLEARCOLORPROC) GetProcAddress(opengl, "glClearColor"))
        && (GlDisable = (PFNGLDISABLEPROC) GetProcAddress(opengl, "glDisable"))
        && (GlDrawElements = (PFNGLDRAWELEMENTSPROC) GetProcAddress(opengl, "glDrawElements"))
        && (GlEnable = (PFNGLENABLEPROC) GetProcAddress(opengl, "glEnable"))
        && (GlGenTextures = (PFNGLGENTEXTURESPROC) GetProcAddress(opengl, "glGenTextures"))
        && (GlPixelStorei = (PFNGLPIXELSTOREIPROC) GetProcAddress(opengl, "glPixelStorei"))
        && (GlPolygonMode = (PFNGLPOLYGONMODEPROC) GetProcAddress(opengl, "glPolygonMode"))
        && (GlScissor = (PFNGLSCISSORPROC) GetProcAddress(opengl, "glScissor"))
        && (GlTexImage2D = (PFNGLTEXIMAGE2DPROC) GetProcAddress(opengl, "glTexImage2D"))
        && (GlTexParameteri = (PFNGLTEXPARAMETERIPROC) GetProcAddress(opengl, "glTexParameteri"))
        && (GlViewport = (PFNGLVIEWPORTPROC) GetProcAddress(opengl, "glViewport"))
    );
}

internal bool ImGuiGl2Init()
{
    return (true
        && (GlCreateShader = (PFNGLCREATESHADERPROC) WglGetProcAddress("glCreateShader"))
        && (GlShaderSource = (PFNGLSHADERSOURCEPROC) WglGetProcAddress("glShaderSource"))
        && (GlCompileShader = (PFNGLCOMPILESHADERPROC) WglGetProcAddress("glCompileShader"))
        && (GlDeleteShader = (PFNGLDELETESHADERPROC) WglGetProcAddress("glDeleteShader"))
        && (GlDetachShader = (PFNGLDETACHSHADERPROC) WglGetProcAddress("glDetachShader"))
        && (GlGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC) WglGetProcAddress("glGetShaderInfoLog"))
        && (GlCreateProgram = (PFNGLCREATEPROGRAMPROC) WglGetProcAddress("glCreateProgram"))
        && (GlAttachShader = (PFNGLATTACHSHADERPROC) WglGetProcAddress("glAttachShader"))
        && (GlLinkProgram = (PFNGLLINKPROGRAMPROC) WglGetProcAddress("glLinkProgram"))
        && (GlUseProgram = (PFNGLUSEPROGRAMPROC) WglGetProcAddress("glUseProgram"))
        && (GlGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC) WglGetProcAddress("glGetProgramInfoLog"))
        && (GlDeleteProgram = (PFNGLDELETEPROGRAMPROC) WglGetProcAddress("glDeleteProgram"))
        && (GlGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC) WglGetProcAddress("glGetUniformLocation"))
        && (GlUniform1i = (PFNGLUNIFORM1IPROC) WglGetProcAddress("glUniform1i"))
        && (GlUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC) WglGetProcAddress("glUniformMatrix4fv"))
        && (GlGetAttribLocation = (PFNGLGETATTRIBLOCATIONPROC) WglGetProcAddress("glGetAttribLocation"))
        && (GlVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC) WglGetProcAddress("glVertexAttribPointer"))
        && (GlEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC) WglGetProcAddress("glEnableVertexAttribArray"))
        && (GlGenBuffers = (PFNGLGENBUFFERSPROC) WglGetProcAddress("glGenBuffers"))
        && (GlBindBuffer = (PFNGLBINDBUFFERPROC) WglGetProcAddress("glBindBuffer"))
        && (GlBufferData = (PFNGLBUFFERDATAPROC) WglGetProcAddress("glBufferData"))
        && (GlBufferSubData = (PFNGLBUFFERSUBDATAPROC) WglGetProcAddress("glBufferSubData"))
        && (GlDeleteBuffers = (PFNGLDELETEBUFFERSPROC) WglGetProcAddress("glDeleteBuffers"))
        && (GlActiveTexture = (PFNGLACTIVETEXTUREPROC) WglGetProcAddress("glActiveTexture"))
    );
}

//#ImGuiRenderer---------------------------------------------------------

struct ImGuiRenderer
{
    GlHandle fontTexture;
    GlHandle vertexBuffer;
    GlHandle indexBuffer;
    GlHandle program;

    int STexture;
    int UModelViewProj;
    int APosition;
    int ATexCoord;
    int AColor;

    Color4F32 clearColor;
    GlHandle currentTexture;
};

internal ImGuiRenderer renderer;

internal const char* vShaderSource = R"(
    #version 120

    attribute vec4 APosition;
    attribute vec2 ATexCoord;
    attribute vec4 AColor;

    varying vec2 ITexCoord;
    varying vec4 IColor;

    uniform mat4 UModelViewProj;

    void main()
    {
        gl_Position = UModelViewProj * APosition;
        ITexCoord = ATexCoord;
        IColor = AColor;
    }
)";

internal const char* fShaderSource = R"(
    #version 120

    varying vec2 ITexCoord;
    varying vec4 IColor;

    uniform sampler2D STexture;

    void main()
    {
        vec4 sample = texture2D(STexture, ITexCoord);
        gl_FragColor = sample.a * IColor;
    }
)";

internal ImGuiStatus ImGuiRendererInit(ImGuiRenderer* renderer)
{
    //Buffers
    {
        GlGenBuffers(1, &renderer->vertexBuffer);
        GlBindBuffer(GL_ARRAY_BUFFER, renderer->vertexBuffer);
        GlBindBuffer(GL_ARRAY_BUFFER, 0);
        GlGenBuffers(1, &renderer->indexBuffer);
        GlBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer->indexBuffer);
        GlBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    //Shader
    {
        GlHandle vShader = GlCreateShader(GL_VERTEX_SHADER);
        GlShaderSource(vShader, 1, &vShaderSource, NULL);
        GlCompileShader(vShader);

        GlHandle fShader = GlCreateShader(GL_FRAGMENT_SHADER);
        GlShaderSource(fShader, 1, &fShaderSource, NULL);
        GlCompileShader(fShader);

        renderer->program = GlCreateProgram();
        GlAttachShader(renderer->program, vShader);
        GlAttachShader(renderer->program, fShader);
        GlLinkProgram(renderer->program);
        GlDetachShader(renderer->program, vShader);
        GlDetachShader(renderer->program, fShader);
        GlDeleteShader(vShader);
        GlDeleteShader(fShader);

        renderer->STexture = GlGetUniformLocation(renderer->program, "STexture");
        renderer->UModelViewProj = GlGetUniformLocation(renderer->program, "UModelViewProj");
        renderer->APosition = GlGetAttribLocation(renderer->program, "APosition");
        renderer->ATexCoord = GlGetAttribLocation(renderer->program, "ATexCoord");
        renderer->AColor = GlGetAttribLocation(renderer->program, "AColor");
    }

    //Texture
    {
        ImGuiIO& io = ImGui::GetIO();
        u8* pixels;
        i32 width, height;
        io.Fonts->GetTexDataAsAlpha8(&pixels, &width, &height);

        GlGenTextures(1, &renderer->fontTexture);
        GlBindTexture(GL_TEXTURE_2D, renderer->fontTexture);
        GlTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        GlTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        GlPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
        GlTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, pixels);

        io.Fonts->TexID = (ImTextureID)(intptr_t) renderer->fontTexture;
    }

    renderer->clearColor = { 0.0f, 1.0f, 1.0f, 1.0f };

    // Setup back-end capabilities flags
    ImGuiIO& io = ImGui::GetIO();
    io.BackendRendererName = "ImGuiRendererGL2";

    ImGuiStatus status = (
        renderer->fontTexture &&
        renderer->vertexBuffer &&
        renderer->indexBuffer &&
        renderer->program) ?
        (IMGUISTATUS_OK) : (IMGUISTATUS_ERROR_RENDERER_INIT);

    return status;
}

internal void ImGuiRendererFinish(ImGuiRenderer* renderer)
{
    //NOTE : dirty exit is ok
}

internal void ImGuiRendererMakeOrthoMatrix(ImDrawData* drawData, f32* matrix)
{
    f32 L = drawData->DisplayPos.x;
    f32 R = drawData->DisplayPos.x + drawData->DisplaySize.x;
    f32 T = drawData->DisplayPos.y;
    f32 B = drawData->DisplayPos.y + drawData->DisplaySize.y;
    //Base X
    matrix[0] = 2.0f / (R - L);
    matrix[1] = 0.0f;
    matrix[2] = 0.0f;
    matrix[3] = 0.0f;
    //Base Y
    matrix[4] = 0.0f;
    matrix[5] = 2.0f / (T - B);
    matrix[6] = 0.0f;
    matrix[7] = 0.0f;
    //Base Z
    matrix[8] = 0.0f;
    matrix[9] = 0.0f;
    matrix[10] = -1.0f;
    matrix[11] = 0.0f;
    //Base W (Translate)
    matrix[12] = (R + L) / (L - R);
    matrix[13] = (T + B) / (B - T);
    matrix[14] = 0.0f;
    matrix[15] = 1.0f;
}

internal void ImGuiRendererSetupPipeline(ImGuiRenderer* renderer, ImDrawData* drawData)
{
    Size2U32 bufferSize = {
        drawData->DisplaySize.x * drawData->FramebufferScale.x,
        drawData->DisplaySize.y * drawData->FramebufferScale.y
    };

    //Setup framebuffer
    GlViewport(0, 0, bufferSize.width, bufferSize.height);
    GlScissor(0, 0, bufferSize.width, bufferSize.height);
    GlClearColor(renderer->clearColor.r, renderer->clearColor.g, renderer->clearColor.b, renderer->clearColor.a);
    GlClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    GlEnable(GL_SCISSOR_TEST);

    //Setup shader
    GlUseProgram(renderer->program);
    //constants
    GlUniform1i(renderer->STexture, 0);
    f32 ortho[16];
    ImGuiRendererMakeOrthoMatrix(drawData, ortho);
    GlUniformMatrix4fv(renderer->UModelViewProj, 1, false, ortho);
    //vertex puller
    GlBindBuffer(GL_ARRAY_BUFFER, renderer->vertexBuffer);
    GlEnableVertexAttribArray(renderer->APosition);
    GlVertexAttribPointer(renderer->APosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, pos));
    GlEnableVertexAttribArray(renderer->ATexCoord);
    GlVertexAttribPointer(renderer->ATexCoord, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, uv));
    GlEnableVertexAttribArray(renderer->AColor);
    GlVertexAttribPointer(renderer->AColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, col));
    GlBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer->indexBuffer);
    //textures
    GlActiveTexture(GL_TEXTURE0);
    GlBindTexture(GL_TEXTURE_2D, renderer->fontTexture);
    renderer->currentTexture = renderer->fontTexture;

    //Setup pipeline
    GlEnable(GL_BLEND);
    GlBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    GlDisable(GL_CULL_FACE);
    GlDisable(GL_DEPTH_TEST);
    GlPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

internal void ImGuiRendererUseTexture(ImGuiRenderer* renderer, GlHandle texture)
{
    if (renderer->currentTexture != texture)
    {
        GlBindTexture(GL_TEXTURE_2D, texture);
        renderer->currentTexture = texture;
    }
}

internal void ImGuiRendererUpdate(ImGuiRenderer* renderer, ImDrawData* drawData)
{
    ImGuiRendererSetupPipeline(renderer, drawData);

    // Render command lists
    for (int n = 0; n < drawData->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = drawData->CmdLists[n];
        const ImDrawVert* vtx_buffer = cmd_list->VtxBuffer.Data;
        const ImDrawIdx* idx_buffer = cmd_list->IdxBuffer.Data;

        u32 vboSize = (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert);
        u32 iboSize = (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx);

        GlBufferData(GL_ARRAY_BUFFER, vboSize, (const GLvoid*) cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);
        GlBufferData(GL_ELEMENT_ARRAY_BUFFER, iboSize, (const GLvoid*) cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback)
            {
                // User callback, registered via ImDrawList::AddCallback()
                // (ImDrawCallback_ResetRenderState is a special callback value used by the user to request the renderer to reset render state.)
                if (pcmd->UserCallback == ImDrawCallback_ResetRenderState)
                    ImGuiRendererSetupPipeline(renderer, drawData);
                else
                    pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                ImGuiRendererUseTexture(renderer, (GlHandle) pcmd->TextureId);
                GlDrawElements(
                    GL_TRIANGLES, 
                    (GLsizei) pcmd->ElemCount, 
                    sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, 
                    (void*)(intptr_t)(pcmd->IdxOffset * sizeof(ImDrawIdx))
                );
            }
        }
    }
}

//#ImGuiPlatform----------------------------------------------------
struct ImGuiPlatform
{
    HINSTANCE appHandle;
    WNDCLASSEX windowClass;
    HWND windowHandle;
    HDC windowDC;
    HGLRC windowRC;
    u64 counterFreq;
    u64 counterAtLastUpdate;
    bool isWindowActive;

    ImGuiMouseCursor currentCursor;
    ImGuiMouseCursor defaultCursor;
    HCURSOR cursorTable[ImGuiMouseCursor_COUNT];
};
internal LRESULT CALLBACK MessageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

//internal ImGuiPlatform platform;

internal ImGuiStatus ImGuiPlatformInit(ImGuiPlatform* platform)
{
    IMGUI_CHECKVERSION();
    ImGuiContext* ctx = ImGui::CreateContext();
    ImGui::SetCurrentContext(ctx);
    ImGui::StyleColorsDark();

    platform->appHandle = GetModuleHandle(NULL);

    //Cursors init
    {
        //NOTE : hInstancle HAS to be NULL if you load predefined cursors
        platform->cursorTable[ImGuiMouseCursor_Arrow] = LoadCursor(NULL, IDC_ARROW);
        platform->cursorTable[ImGuiMouseCursor_TextInput] = LoadCursor(NULL, IDC_IBEAM);
        platform->cursorTable[ImGuiMouseCursor_ResizeAll] = LoadCursor(NULL, IDC_SIZEALL);
        platform->cursorTable[ImGuiMouseCursor_ResizeNS] = LoadCursor(NULL, IDC_SIZENS);
        platform->cursorTable[ImGuiMouseCursor_ResizeEW] = LoadCursor(NULL, IDC_SIZEWE);
        platform->cursorTable[ImGuiMouseCursor_ResizeNESW] = LoadCursor(NULL, IDC_SIZENESW);
        platform->cursorTable[ImGuiMouseCursor_ResizeNWSE] = LoadCursor(NULL, IDC_SIZENWSE);
        platform->cursorTable[ImGuiMouseCursor_Hand] = LoadCursor(NULL, IDC_HAND);
        platform->cursorTable[ImGuiMouseCursor_NotAllowed] = LoadCursor(NULL, IDC_NO);

        platform->currentCursor = ImGuiMouseCursor_Arrow;
        platform->defaultCursor = ImGuiMouseCursor_Arrow;
        SetCursor(platform->cursorTable[ImGuiMouseCursor_Arrow]);
    }

    //Time init
    {
        LARGE_INTEGER freq = {};
        QueryPerformanceFrequency(&freq);
        LARGE_INTEGER counter = {};
        QueryPerformanceCounter(&counter);

        platform->counterFreq = (u64)freq.QuadPart;
        platform->counterAtLastUpdate = (u64)counter.QuadPart;
    }

    //Window class init
    {
        WNDCLASSEX clazz = {};
        clazz.cbSize = sizeof(WNDCLASSEX);
        clazz.lpszClassName = "MainWindowClass";
        clazz.style = (CS_HREDRAW | CS_VREDRAW);
        clazz.lpfnWndProc = MessageHandler;
        clazz.hInstance = platform->appHandle;

        RegisterClassEx(&clazz);
        platform->windowClass = clazz;
    }

    //WGL init
    bool loaded = ImGuiWglInit();

    //Window init 
    platform->windowHandle = CreateWindowA(
        platform->windowClass.lpszClassName,
        "ImGuiApp",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT,
        640, 480,
        NULL, NULL, platform->appHandle, &platform
    );
      
    if (platform->windowHandle)
    {
        SetWindowLongPtr(platform->windowHandle, GWLP_USERDATA, (LONG_PTR) platform);
        platform->windowDC = GetWindowDC(platform->windowHandle);

        PIXELFORMATDESCRIPTOR pixelAttribs = {};
        pixelAttribs.nSize = sizeof(PIXELFORMATDESCRIPTOR);
        pixelAttribs.nVersion = 1;
        pixelAttribs.dwFlags = (PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER);
        pixelAttribs.iPixelType = PFD_TYPE_RGBA;
        pixelAttribs.cColorBits = 32;
        pixelAttribs.cDepthBits = 24;
        pixelAttribs.cStencilBits = 8;
        pixelAttribs.iLayerType = PFD_MAIN_PLANE;
        i32 pixelFormat = ChoosePixelFormat(platform->windowDC, &pixelAttribs);

        if (SetPixelFormat(platform->windowDC, pixelFormat, &pixelAttribs))
        {
            platform->windowRC = WglCreateContext(platform->windowDC);
            WglMakeCurrent(platform->windowDC, platform->windowRC);

            bool loaded = ImGuiGl1Init();
            loaded = ImGuiGl2Init();
            if (platform->windowRC)
            {
                ImGuiIO& io = ImGui::GetIO();
                io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
                io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
                io.BackendPlatformName = "ImGuiPlatformWin32";
                io.ImeWindowHandle = platform->windowHandle;

                io.KeyMap[ImGuiKey_Tab] = VK_TAB;
                io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
                io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
                io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
                io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
                io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
                io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
                io.KeyMap[ImGuiKey_Home] = VK_HOME;
                io.KeyMap[ImGuiKey_End] = VK_END;
                io.KeyMap[ImGuiKey_Insert] = VK_INSERT;
                io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
                io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
                io.KeyMap[ImGuiKey_Space] = VK_SPACE;
                io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
                io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
                io.KeyMap[ImGuiKey_KeyPadEnter] = VK_RETURN;
                io.KeyMap[ImGuiKey_A] = 'A';
                io.KeyMap[ImGuiKey_C] = 'C';
                io.KeyMap[ImGuiKey_V] = 'V';
                io.KeyMap[ImGuiKey_X] = 'X';
                io.KeyMap[ImGuiKey_Y] = 'Y';
                io.KeyMap[ImGuiKey_Z] = 'Z';
            }
            else { return IMGUISTATUS_ERROR_GL_CONTEXT; }
        }
        else { return IMGUISTATUS_ERROR_PIXEL_FORMAT; }
    }
    else { return IMGUISTATUS_ERROR_WINDOW_HANDLE; }

    return IMGUISTATUS_OK;
}

internal void ImGuiPlatformFinish(ImGuiPlatform* platform)
{
    //NOTE : dirty exit is ok
}

internal void ImGuiPlatformSetMouseCursor(ImGuiPlatform* platform, ImGuiMouseCursor cursor)
{
    if (platform->currentCursor != cursor)
    {
        if (cursor >= ImGuiMouseCursor_COUNT)
        {
            cursor = platform->defaultCursor;
        }
        platform->currentCursor = cursor;
        HCURSOR cursorHandle = (cursor == ImGuiMouseCursor_None) ?
            (NULL) :
            (platform->cursorTable[cursor]);
        SetCursor(cursorHandle);
    }
}

internal void ImGuiPlatformUpdate(ImGuiPlatform* platform)
{
    ImGuiIO& io = ImGui::GetIO();

    RECT rect;
    GetClientRect(platform->windowHandle, &rect);
    io.DisplaySize = ImVec2((f32) (rect.right - rect.left), (f32) (rect.bottom - rect.top));

    LARGE_INTEGER currentCounter = {};
    QueryPerformanceCounter(&currentCounter);
    u64 deltaTicks = ((u64) currentCounter.QuadPart) - platform->counterAtLastUpdate;
    platform->counterAtLastUpdate = (u64) currentCounter.QuadPart;
    f64 deltaSec = (deltaTicks / (f64) platform->counterFreq);
    io.DeltaTime = deltaSec;

    io.KeyCtrl = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
    io.KeyShift = (GetKeyState(VK_SHIFT) & 0x8000) != 0;
    io.KeyAlt = (GetKeyState(VK_MENU) & 0x8000) != 0;
    io.KeySuper = false;

    //Update mouse position
    {
        if (io.WantSetMousePos)
        {
            POINT pos = { (LONG) io.MousePos.x, (LONG) io.MousePos.y };
            ClientToScreen(platform->windowHandle, &pos);
            SetCursorPos(pos.x, pos.y);
        }

        POINT pos;
        io.MousePos = (GetCursorPos(&pos) && ScreenToClient(platform->windowHandle, &pos)) ?
            (ImVec2((f32)pos.x, (f32)pos.y)) :
            (ImVec2(-FLT_MAX, -FLT_MAX));
    }
} 

internal LRESULT CALLBACK MessageHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_ACTIVATE : 
        {
            ImGuiPlatform* platform = (ImGuiPlatform*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
            platform->isWindowActive = !(LOWORD(wParam) == WA_INACTIVE);
        }
        break;

        case WM_CLOSE :
            PostQuitMessage(IMGUISTATUS_OK);
        break;

        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:
        case WM_XBUTTONDOWN:
        case WM_LBUTTONDBLCLK:
        case WM_RBUTTONDBLCLK:
        case WM_MBUTTONDBLCLK:
        case WM_XBUTTONDBLCLK:
        {
            int button = 0;
            if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) { button = 0; }
            if (msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) { button = 1; }
            if (msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) { button = 2; }
            if (msg == WM_XBUTTONDOWN || msg == WM_XBUTTONDBLCLK) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
            if (!ImGui::IsAnyMouseDown() && GetCapture() == NULL)
            {
                SetCapture(hwnd);
            }
            ImGui::GetIO().MouseDown[button] = true;
        }
        return TRUE;

        case WM_LBUTTONUP:
        case WM_RBUTTONUP:
        case WM_MBUTTONUP:
        case WM_XBUTTONUP:
        {
            int button = 0;
            if (msg == WM_LBUTTONUP) { button = 0; }
            if (msg == WM_RBUTTONUP) { button = 1; }
            if (msg == WM_MBUTTONUP) { button = 2; }
            if (msg == WM_XBUTTONUP) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? 3 : 4; }
            ImGui::GetIO().MouseDown[button] = false;
            if (!ImGui::IsAnyMouseDown() && ::GetCapture() == hwnd)
            {
                ReleaseCapture();
            }
        }
        return TRUE;

        case WM_MOUSEWHEEL:
            ImGui::GetIO().MouseWheel += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
        return TRUE;

        case WM_MOUSEHWHEEL:
            ImGui::GetIO().MouseWheelH += (float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA;
        return TRUE;

        case WM_KEYDOWN:
        case WM_SYSKEYDOWN:
            if (wParam < 256) ImGui::GetIO().KeysDown[wParam] = 1;
        return TRUE;

        case WM_KEYUP:
        case WM_SYSKEYUP:
            if (wParam < 256) ImGui::GetIO().KeysDown[wParam] = 0;
        return 0;

        case WM_CHAR:
            // You can also use ToAscii()+GetKeyboardState() to retrieve characters.
            if (wParam > 0 && wParam < 0x10000) ImGui::GetIO().AddInputCharacterUTF16((unsigned short)wParam);
        return TRUE;

        //NOTE : https://devblogs.microsoft.com/oldnewthing/20061121-15/?p=28943
        case WM_SETCURSOR:
        {
            ImGuiPlatform* platform = (ImGuiPlatform*) GetWindowLongPtr(hwnd, GWLP_USERDATA);

            bool hit = (LOWORD(lParam) == HTCLIENT);
            bool swCursor = ImGui::GetIO().MouseDrawCursor;
            if (hit)
            {
                ImGuiMouseCursor cursor = (swCursor) ? (ImGuiMouseCursor_None) : (ImGui::GetMouseCursor());
                ImGuiPlatformSetMouseCursor(platform, cursor);
                return TRUE;
            }
            else
            {
                //NOTE : default winproc will set it, invalidate
                platform->currentCursor = ImGuiMouseCursor_None;
            }
        }
        break;
    }

    return DefWindowProc(hwnd, msg, wParam, lParam);
}

internal void ExitWithError(ImGuiStatus status)
{
    char message[4096];
    snprintf(
        message,
        sizeof(message),
        "Initialization error : %s.\nApplication will now exit.",
        ImGuiStatusToString[status]
    );

    MessageBox(NULL, message, "Error", MB_OK | MB_ICONERROR);
    exit((int) status);
}

int main(int argc, char** args)
{
    ImGuiPlatform platform;

    ImGuiStatus status = ImGuiPlatformInit(&platform);
    if (status)
    {
        ExitWithError(status);
    }

    status = ImGuiRendererInit(&renderer);
    if (status)
    {
        ExitWithError(status);
    }

    renderer.clearColor = { 0.0f, 0.0f, 0.0f, 1.0f };
    ImGui::StyleColorsClassic();

    ImGuiAppInit();
    ShowWindow(platform.windowHandle, SW_SHOW);
    UpdateWindow(platform.windowHandle);

    bool isRunning = true;
    while (isRunning)
    {
        //Handle message queue
        {
            MSG message = {};
            //NOTE pass in NULL to process all thread and window messagess
            while (PeekMessageA(&message, NULL, 0, 0, PM_REMOVE))
            {
                if (message.message == WM_QUIT)
                {
                    isRunning = false;
                }

                TranslateMessage(&message);
                DispatchMessage(&message);
            }

            ImGuiPlatformUpdate(&platform);

            if (platform.isWindowActive)
            {
                ImGui::NewFrame();
                ImGuiAppUpdate();
                ImGui::EndFrame();
                ImGui::Render();
                ImGuiRendererUpdate(&renderer, ImGui::GetDrawData());
                //Vsync is on this limits update rate no sleep is needed
                SwapBuffers(platform.windowDC);
            }
            else
            {
                Sleep(10);
            }
        }
    }

    ImGuiAppFinish();
    ImGuiRendererFinish(&renderer);
    ImGuiPlatformFinish(&platform);

    return 0;
}