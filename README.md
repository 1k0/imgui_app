# README #

### About ###

ImGui win32 platform layer(platform + renderer + main loop), for fast tool creation.

### Build ###

Copy files to your project and add build_imgui.cpp to the build list.
Include imgui_app.h and implement its declared extern functions.
