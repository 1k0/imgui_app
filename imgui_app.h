#pragma once

#include "imgui/imgui.h"

extern void ImGuiAppInit();
extern void ImGuiAppFinish();
extern void ImGuiAppUpdate();